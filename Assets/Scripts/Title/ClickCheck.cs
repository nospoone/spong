﻿using UnityEngine;
using System.Collections;

public class ClickCheck : MonoBehaviour {

    public Fader fader;
    public AudioManager audioManager;

    private bool playedHoverSound = false;

	// Use this for initialization
	void Awake () {
        fader.Fade(0, 1);
	}
	
	// Update is called once per frame
	void Update () {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo)) {
            if (hitInfo.transform.name == "2 Players" || hitInfo.transform.name == "1 Player (VS Hard)" || hitInfo.transform.name == "1 Player (VS Easy)" || hitInfo.transform.name == "Single Player") {
                if (!playedHoverSound) {
                    audioManager.hover.Play();
                    playedHoverSound = true;
                }
                iTween.ColorTo(hitInfo.transform.gameObject, Color.cyan, 0.1f);
                if (Input.GetMouseButtonDown(0)) {
                    audioManager.confirm.Play();
                    audioManager.fadeOutBGM();
                    Registry.gameInProgress = true;
                    switch (hitInfo.transform.name) {
                        case "2 Players":
                            audioManager.twoPlayers.Play();
                            Registry.AIEnabled = false;
                            Registry.gameType = Registry.GameType.Multiplayer;
                            fader.Fade(1, 1);
                            StartCoroutine(ChangeScene());
                            break;
                        case "1 Player (VS Easy)":
                            audioManager.easy.Play();
                            Registry.AIEnabled = true;
                            Registry.gameType = Registry.GameType.Multiplayer;
                            Registry.AIDifficulty = AI.Difficulty.Easy;
                            fader.Fade(1, 1);
                            StartCoroutine(ChangeScene());
                            break;
                        case "1 Player (VS Hard)":
                            audioManager.hard.Play();
                            Registry.AIEnabled = true;
                            Registry.gameType = Registry.GameType.Multiplayer;
                            Registry.AIDifficulty = AI.Difficulty.Hard;
                            fader.Fade(1, 1);
                            StartCoroutine(ChangeScene());
                            break;
                        case "Single Player":
                            audioManager.singlePlayer.Play();
                            Registry.AIEnabled = false;
                            Registry.gameType = Registry.GameType.Singleplayer;                            
                            fader.Fade(1, 1);
                            StartCoroutine(ChangeScene());
                            break;
                        default:
                            break;
                    }
                }
            } else {
                playedHoverSound = false;
                iTween.ColorTo(GameObject.Find("2 Players"), Color.white, 0.1f);
                iTween.ColorTo(GameObject.Find("1 Player (VS Hard)"), Color.white, 0.1f);
                iTween.ColorTo(GameObject.Find("1 Player (VS Easy)"), Color.white, 0.1f);
                iTween.ColorTo(GameObject.Find("Single Player"), Color.white, 0.1f);
            }
        }
	}

    public IEnumerator ChangeScene() {
        for (float timer = 1; timer >= 0; timer -= Time.deltaTime)
            yield return 0;

        Application.LoadLevel("Play");
    }
}
