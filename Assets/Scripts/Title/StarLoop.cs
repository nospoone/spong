﻿using UnityEngine;
using System.Collections;

public class StarLoop : MonoBehaviour {

    public float speed = 0.01f;
    private Vector3 positionDisplacement;

	// Use this for initialization
	void Start () {
        positionDisplacement = new Vector3(speed, 0, 0);
	}
	
	// Update is called once per frame
	void Update () {
        if (transform.position.x <= -20) {
            transform.position = new Vector3(20, transform.position.y, transform.position.z);
        }

        transform.position -= positionDisplacement;
	}
}
