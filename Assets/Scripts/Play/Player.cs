﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    public enum Side {
        Left,
        Right
    }

    public Side side;
    public InputManager inputManager;
    public float paddleSpeed = 0.05f;
    public float sprintFactor = 1.005f;

    public Sprite normalSprite;
    public Sprite blurredSprite;

    void Awake() {
        if (Registry.gameType == Registry.GameType.Singleplayer && side == Side.Left) {
            gameObject.SetActive(false);
        }
    }

    void Update() {
        if (side == Side.Left) {
            if (inputManager.leftUpPressed) {
                if (inputManager.leftSprintPressed) {
                    transform.position += new Vector3(0, paddleSpeed * sprintFactor, 0);
                } else {
                    transform.position += new Vector3(0, paddleSpeed, 0);
                }
            }

            if (inputManager.leftDownPressed) {
                if (inputManager.leftSprintPressed) {
                    transform.position -= new Vector3(0, paddleSpeed * sprintFactor, 0);
                } else {
                    transform.position -= new Vector3(0, paddleSpeed, 0);
                }
            }

            if (inputManager.leftUpPressed || inputManager.leftDownPressed) {
                GetComponent<SpriteRenderer>().sprite = blurredSprite;
            } else {
                GetComponent<SpriteRenderer>().sprite = normalSprite;
            }
        } else if (side == Side.Right) {
            if (inputManager.rightUpPressed) {
                if (inputManager.rightSprintPressed) {
                    transform.position += new Vector3(0, paddleSpeed * sprintFactor, 0);
                } else {
                    transform.position += new Vector3(0, paddleSpeed, 0);
                }
            }

            if (inputManager.rightDownPressed) {
                if (inputManager.rightSprintPressed) {
                    transform.position -= new Vector3(0, paddleSpeed * sprintFactor, 0);
                } else {
                    transform.position -= new Vector3(0, paddleSpeed, 0);
                }
            }

            if (inputManager.rightUpPressed || inputManager.rightDownPressed) {
                GetComponent<SpriteRenderer>().sprite = blurredSprite;
            } else {
                GetComponent<SpriteRenderer>().sprite = normalSprite;
            }
        }

        transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y, -3.3f, 3.3f), transform.position.z);
    }

    void OnCollisionEnter(Collision collision) {
        animation.Play();
    }
}
