﻿using UnityEngine;
using System.Collections;

public class CameraZoom : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Zoom(float value, float time) {
        Hashtable tweenParameters = new Hashtable();
        tweenParameters.Add("from", Camera.main.orthographicSize);
        tweenParameters.Add("to", value);
        tweenParameters.Add("time", time);
        tweenParameters.Add("easetype", iTween.EaseType.easeOutQuad);
        tweenParameters.Add("onupdate", "ZoomUpdate");
        iTween.ValueTo(gameObject, tweenParameters);
    }

    public void ZoomUpdate(float value) {
        Camera.main.orthographicSize = value;
    }
}
