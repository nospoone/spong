﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour {

    public float duration;
    public float strength;
    public Vector2 offset = Vector2.zero;
    public Vector2 origin = Vector2.zero;

    public bool shaking = false;

	// Update is called once per frame
	void Update () {
        Camera.main.transform.position = new Vector3(Camera.main.transform.position.x - offset.x, Camera.main.transform.position.y - offset.y, Camera.main.transform.position.z);

        if (duration > 0) {
            duration -= Time.deltaTime;
            if (duration <= 0) {
                shaking = false;
                offset = Vector2.zero;
            } else {
                shaking = true;
                offset.x = Random.Range(0, 1.0f) * strength * Camera.main.pixelWidth * 2 - strength * Camera.main.pixelWidth;
                offset.y = Random.Range(0, 1.0f) * strength * Camera.main.pixelHeight * 2 - strength * Camera.main.pixelHeight;
            }
        }

        Camera.main.transform.position = new Vector3(Camera.main.transform.position.x + offset.x, Camera.main.transform.position.y + offset.y, Camera.main.transform.position.z);
	}
}
