﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class ParallaxBackground : MonoBehaviour
{
	[Range(0, 1)]
	public float HorizontalFactor = 0;
	[Range(0, 1)]
	public float VerticalFactor = 0;
	public bool FollowCamera = true;
	public Camera cam;

	private Vector3 initialPosition = new Vector3();
	private Vector3 bgPosition = new Vector3();

	void Awake() {
		initialPosition.x = cam.transform.position.x;
		initialPosition.y = cam.transform.position.y;
        bgPosition.z = transform.position.z;
	}
	
	void Update() {
		if (FollowCamera) {
			bgPosition.x = (cam.transform.position.x - initialPosition.x) * HorizontalFactor;
			bgPosition.y = (cam.transform.position.y - initialPosition.y) * VerticalFactor;
		} else {
			bgPosition.x = (initialPosition.x - cam.transform.position.x) * HorizontalFactor;
			bgPosition.y = (initialPosition.y - cam.transform.position.y) * VerticalFactor;
		}

		transform.position = bgPosition;
	}
}