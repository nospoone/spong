﻿using UnityEngine;
using System.Collections;

public class SmoothCamera2D : MonoBehaviour {

    public float dampTime = 0.15f;
    private Vector3 velocity = Vector3.zero;
    public Transform target;

    // Update is called once per frame
    void Update() {
        if (target) {
            Vector3 point = camera.WorldToViewportPoint(target.position);
            Vector3 delta = target.position - camera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z)); //(new Vector3(0.5, 0.5, point.z));
            Vector3 destination = new Vector3(Mathf.Clamp(transform.position.x + delta.x, -0.15f, 0.15f), Mathf.Clamp(transform.position.y + delta.y, -0.2f, 0.2f) + 0.2f, -10);        

            transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);
        }
    }
}