﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {

    private Vector3 direction;

    public float speed;
    public CameraShake cameraShake;
    public ScoreboardManager scoreboardManager;
    public AudioManager audioManager;
    public Timer timer;

    private float currentSpeed = 0;

    void Start() {
        StartCoroutine(LaunchCountdown());
    }

    void FixedUpdate() {
        direction.z = 0;
        transform.position += direction * currentSpeed;
    }

    void OnCollisionEnter(Collision collision) {
        Vector3 normal = collision.contacts[0].normal;
        direction = Vector3.Reflect(direction, normal);

        string tag = collision.transform.tag;

        if (tag == "Paddle") {
            currentSpeed += 0.01f;
            audioManager.paddleHit.Play();

            float minPosition = collision.contacts[0].otherCollider.transform.position.y - collision.contacts[0].otherCollider.renderer.bounds.size.y / 2;
            float maxPosition = collision.contacts[0].otherCollider.transform.position.y + collision.contacts[0].otherCollider.renderer.bounds.size.y / 2;
            float paddlePercent = map(collision.contacts[0].point.y, minPosition, maxPosition, -0.5f, 0.5f);

            direction.y += paddlePercent;

            if (scoreboardManager.ballBounces % 5 == 0 && scoreboardManager.ballBounces != 0 && !scoreboardManager.ballSpawned) {
                Instantiate(this, new Vector3(0, 0, -3), Quaternion.identity);
                scoreboardManager.ballSpawned = true;
                audioManager.extraBall.Play();
                Debug.Log("?");
            }
        }

        if (tag == "Left Goal" || tag == "Right Goal") {
            audioManager.score.Play();
            if (Random.Range(0f, 1f) > 0.5f) {
                audioManager.boom1.Play();
            } else {
                audioManager.boom2.Play();
            }
            transform.Find("Explosion").GetComponent<ParticleSystem>().Emit(transform.Find("Explosion").GetComponent<ParticleSystem>().maxParticles);
            GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<TrailRenderer>().enabled = false;
            currentSpeed = 0;
            transform.position = new Vector3(0, 0, -3);

            if (Registry.gameType == Registry.GameType.Multiplayer) {
                if (tag == "Left Goal") {
                    scoreboardManager.ScorePoint(Player.Side.Right);
                } else {
                    scoreboardManager.ScorePoint(Player.Side.Left);
                }
            } else {
                scoreboardManager.RemoveLife();
            }

            if (Registry.gameInProgress) {
                Respawn();
            }

            cameraShake.duration = 0.3f;
            cameraShake.strength = 0.000025f;
        }

        if (tag == "Wall") {
            audioManager.wallHit.Play();
            if (collision.gameObject.name == "Left") {
                scoreboardManager.UpdateBounceCounter();
            }
        }

        if (tag == "Left Goal" || tag == "Right Goal" || tag == "Wall") {
            collision.gameObject.GetComponent<AnimationLinker>().linkedAnimation.Play();
        }
    }

    public void Respawn() {
        transform.rotation = Quaternion.Euler(90, 90, 0);
        iTween.RotateTo(gameObject, new Vector3(0, 0, 0), 3);
        GetComponent<SpriteRenderer>().enabled = true;

        if (Registry.gameType == Registry.GameType.Multiplayer) { 
            timer.ResetTimer();
        }

        StartCoroutine(LaunchCountdown());
    }

    void Launch() {
        direction = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), 0).normalized;

        if (Mathf.Abs(direction.y) > Mathf.Abs(direction.x)) {
            float x = direction.x;
            direction.x = direction.y;
            direction.y = x;
        }

        audioManager.ballRespawn.Play();
        audioManager.theBallIsLoose.Play();
        currentSpeed = speed;
    }

    public IEnumerator LaunchCountdown() {
        for (float timer = 3; timer >= 0; timer -= Time.deltaTime)
            yield return 0;

        GetComponent<TrailRenderer>().enabled = true;
        Launch();
    }

    float map(float s, float a1, float a2, float b1, float b2) {
        return b1 + (s - a1) * (b2 - b1) / (a2 - a1);
    }

}