﻿using UnityEngine;
using System.Collections;

public class Timer : MonoBehaviour {
    [HideInInspector]
    public float time = 0;

    [HideInInspector]
    public bool countdown = false;

    public TextMesh textMesh;

	// Use this for initialization
	void Start () {
        time = 4;
        countdown = true;
        animation.Play();
	}
	
	// Update is called once per frame
	void Update () {
        if (Registry.gameInProgress && !countdown) { 
            time += Time.deltaTime;
            textMesh.text = msToString(Mathf.RoundToInt(time * 1000));
        } else if (Registry.gameInProgress && countdown) {
            time -= Time.deltaTime;
            if (time <= 0) {
                time = 0;
                countdown = false;
            }
            textMesh.text = msToString(Mathf.RoundToInt(time * 1000));
        }
	}

    public string msToString(float millisecs) {
		float seconds = Mathf.Floor(millisecs / 1000);
        float minutes = Mathf.Floor(seconds / 60);
        float hours = Mathf.Floor(minutes / 60);

		seconds %= 60;
		minutes %= 60;

        string outputMinutes = (minutes < 10 ? "0" : "") + minutes;
        string outputSeconds = (seconds < 10 ? "0" : "") + seconds;
        string outputHours = (hours < 10 ? "0" : "") + hours;

        return outputHours + ":" + outputMinutes + ":" + outputSeconds;
	}

    public void ResetTimer() {
        time = 4;
        countdown = true;
        animation.Play();
    }
}
