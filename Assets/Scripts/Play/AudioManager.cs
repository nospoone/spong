﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {

    public AudioSource ballRespawn;
    public AudioSource paddleHit;
    public AudioSource wallHit;
    public AudioSource score;
    public AudioSource hover;
    public AudioSource confirm;
    public AudioSource BGM;

    public AudioSource blueWins;
    public AudioSource boom1;
    public AudioSource boom2;
    public AudioSource easy;
    public AudioSource explosion;
    public AudioSource hard;
    public AudioSource redWins;
    public AudioSource spong;
    public AudioSource secretSFX;
    public AudioSource theBallIsLoose;
    public AudioSource twoPlayers;
    public AudioSource singlePlayer;
    public AudioSource extraBall;
    public AudioSource gameOver;

    public void fadeOutBGM() {
        iTween.AudioTo(BGM.gameObject, 0, 1, 1);
    }
}
