﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InputManager : MonoBehaviour {

    public KeyCode leftPlayerUp = KeyCode.W;
    public KeyCode leftPlayerDown = KeyCode.S;
    public KeyCode leftPlayerSprint = KeyCode.LeftShift;
    public KeyCode rightPlayerUp = KeyCode.UpArrow;
    public KeyCode rightPlayerDown = KeyCode.DownArrow;
    public KeyCode rightPlayerSprint = KeyCode.RightShift;

    [HideInInspector]
    public bool leftUpPressed = false;
    [HideInInspector]
    public bool leftDownPressed = false;
    [HideInInspector]
    public bool leftSprintPressed = false;
    [HideInInspector]
    public bool rightUpPressed = false;
    [HideInInspector]
    public bool rightDownPressed = false;
    [HideInInspector]
    public bool rightSprintPressed = false;

    public bool playerLeftEnabled = false;
    public bool playerRightEnabled = true;

    void Awake() {
        if (Registry.AIEnabled) {
            playerLeftEnabled = false;
        } else {
            playerLeftEnabled = true;
        }
    }

	void Update () {
        if (Registry.gameInProgress) {
            if (playerLeftEnabled) {
                if (Input.GetKey(leftPlayerUp)) {
                    leftUpPressed = true;
                } else {
                    leftUpPressed = false;
                }

                if (Input.GetKey(leftPlayerDown)) {
                    leftDownPressed = true;
                } else {
                    leftDownPressed = false;
                }

                if (Input.GetKey(leftPlayerSprint)) {
                    leftSprintPressed = true;
                } else {
                    leftSprintPressed = false;
                }
            }

            if (playerRightEnabled) {
                if (Input.GetKey(rightPlayerUp)) {
                    rightUpPressed = true;
                } else {
                    rightUpPressed = false;
                }

                if (Input.GetKey(rightPlayerDown)) {
                    rightDownPressed = true;
                } else {
                    rightDownPressed = false;
                }

                if (Input.GetKey(rightPlayerSprint)) {
                    rightSprintPressed = true;
                } else {
                    rightSprintPressed = false;
                }
            }
        } else {
            leftUpPressed = false;
            leftDownPressed = false;
            rightUpPressed = false;
            rightDownPressed = false;
        }
	}
}
