﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AI : MonoBehaviour {

    public enum Difficulty {
        Easy,
        Hard
    }

    public Player.Side side;

    public GameObject leftPaddle;
    public GameObject rightPaddle;
    public GameObject ball;
    public InputManager inputManager;

    public Difficulty difficulty = Difficulty.Easy;

    private Dictionary<Player.Side, GameObject> paddles = new Dictionary<Player.Side, GameObject>();

    void Awake() {
        paddles.Add(Player.Side.Left, leftPaddle);
        paddles.Add(Player.Side.Right, rightPaddle);

        difficulty = Registry.AIDifficulty;

        if (!Registry.AIEnabled) {
            enabled = false;
        }
    }

    void Update() {
        inputManager.leftUpPressed = false;
        inputManager.leftDownPressed = false;

        if (paddles[side].transform.position.y - 0.6f > ball.transform.position.y) {
            inputManager.leftDownPressed = true;
            inputManager.leftUpPressed = false;
        } else if (paddles[side].transform.position.y + 0.6f < ball.transform.position.y) {
            inputManager.leftDownPressed = false;
            inputManager.leftUpPressed = true;
        }
        
        if (difficulty == Difficulty.Hard) {
            if (paddles[side].transform.position.y - 0.6f > ball.transform.position.y || paddles[side].transform.position.y + 0.6f < ball.transform.position.y) {
                inputManager.leftSprintPressed = true;
            } else {
                inputManager.leftSprintPressed = false;
            }
        }
    }

}