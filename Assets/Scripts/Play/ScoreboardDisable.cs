﻿using UnityEngine;
using System.Collections;

public class ScoreboardDisable : MonoBehaviour {

	void Awake () {
        if (Registry.gameType == Registry.GameType.Multiplayer) {
            gameObject.SetActive(false);
        }
	}
}
