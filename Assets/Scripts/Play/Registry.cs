﻿using UnityEngine;
using System.Collections;

public static class Registry {

    public enum GameType {
        Singleplayer,
        Multiplayer
    }

    public static int maximumPoints = 3;
    public static bool gameInProgress = true;
    public static bool AIEnabled = false;
    public static AI.Difficulty AIDifficulty = AI.Difficulty.Easy;
    public static GameType gameType = GameType.Singleplayer;
}
