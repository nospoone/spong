﻿using UnityEngine;
using System.Collections;

public class Counter : MonoBehaviour {

	// Use this for initialization
	void Start () {
        if (Registry.gameType != Registry.GameType.Singleplayer) {
            gameObject.SetActive(false);
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
