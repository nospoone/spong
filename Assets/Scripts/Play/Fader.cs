﻿using UnityEngine;
using System.Collections;

public class Fader : MonoBehaviour {

    public bool changeStartingOpacity = false;
    [Range(0, 1)]
    public float startOpacity = 0;

	// Use this for initialization
	void Start () {
        if (changeStartingOpacity) {
            renderer.material.color = new Color(renderer.material.color.r, renderer.material.color.g, renderer.material.color.b, startOpacity);
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Fade(float value, float time) {
        Hashtable tweenParameters = new Hashtable();
        tweenParameters.Add("from", renderer.material.color.a);
        tweenParameters.Add("to", value);
        tweenParameters.Add("time", time);
        tweenParameters.Add("easetype", iTween.EaseType.easeOutQuad);
        tweenParameters.Add("onupdate", "FadeUpdate");
        iTween.ValueTo(gameObject, tweenParameters);
    }

    public void FadeUpdate(float value) {
        renderer.material.color = new Color(renderer.material.color.r, renderer.material.color.g, renderer.material.color.b, value);
    }
}
