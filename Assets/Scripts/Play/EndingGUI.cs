﻿using UnityEngine;
using System.Collections;

public class EndingGUI : MonoBehaviour {

    public float winnerTextEndScale = 0.025f;
    public float buttonsEndScale = 0.05f;
    public Fader fader;

    public AudioManager audioManager;

    private bool playedHoverSound = false;

    void Update() {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo) && !Registry.gameInProgress) {
            if (hitInfo.transform.name == "Again" || hitInfo.transform.name == "Title") {
                if (!playedHoverSound) {
                    audioManager.hover.Play();
                    playedHoverSound = true;
                }
                iTween.ColorTo(hitInfo.transform.gameObject, Color.cyan, 0.1f);
                if (Input.GetMouseButtonDown(0)) {
                    audioManager.confirm.Play();
                    if (hitInfo.transform.name == "Again") {
                        TriggerReset();
                        GameObject.Find("Scoreboard Manager").GetComponent<ScoreboardManager>().ResetGame();
                    } else {
                        fader.Fade(1, 1);
                        audioManager.fadeOutBGM();
                        StartCoroutine(ChangeLevel());
                    }
                }
            } else {
                playedHoverSound = false;
                iTween.ColorTo(GameObject.Find("Again"), Color.white, 0.1f);
                iTween.ColorTo(GameObject.Find("Title"), Color.white, 0.1f);
            }
        }
    }

    public void TriggerEnding(Player.Side side) {
        if (Registry.gameType == Registry.GameType.Multiplayer) { 
            if (side == Player.Side.Left) {
                transform.Find("Winner").GetComponent<TextMesh>().text = "Blue Wins!";
            } else {
                transform.Find("Winner").GetComponent<TextMesh>().text = "Red Wins!";
            }
        } else if (Registry.gameType == Registry.GameType.Singleplayer) {
            transform.Find("Winner").GetComponent<TextMesh>().text = "Game Over!";
        }

        iTween.ScaleTo(transform.Find("Winner").gameObject, new Vector3(winnerTextEndScale, winnerTextEndScale), 1);
        iTween.ScaleTo(transform.Find("Again").gameObject, new Vector3(buttonsEndScale, buttonsEndScale), 1);
        iTween.ScaleTo(transform.Find("Title").gameObject, new Vector3(buttonsEndScale, buttonsEndScale), 1);
    }

    public void TriggerReset() {
        iTween.ScaleTo(transform.Find("Winner").gameObject, new Vector3(10, 10), 1);
        iTween.ScaleTo(transform.Find("Again").gameObject, new Vector3(10, 10), 1);
        iTween.ScaleTo(transform.Find("Title").gameObject, new Vector3(10, 10), 1);
    }

    public IEnumerator ChangeLevel() {
        for (float timer = 1; timer >= 0; timer -= Time.deltaTime)
            yield return 0;

        Application.LoadLevel("Title");
    }
}
