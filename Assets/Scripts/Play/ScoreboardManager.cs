﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScoreboardManager : MonoBehaviour {

    public GameObject scoreboardLeft;
    public GameObject scoreboardRight;
    public GameObject scoreboardSingleplayer;
    public TextMesh counter;
    public Fader fader;
    public EndingGUI endingGUI;
    public Ball ball;
    public Timer timer;
    public AudioManager audioManager;

    private Dictionary<Player.Side, int> score = new Dictionary<Player.Side, int>() {
        { Player.Side.Left, 0 },
        { Player.Side.Right, 0 }
    };

    private Dictionary<Player.Side, GameObject> scoreboards = new Dictionary<Player.Side, GameObject>();
    private int BallsLeft = 5;
    public int ballBounces = 0;

    // i suck at programming
    public bool ballSpawned;
    private GameObject ballClone;

    public void Awake() {
        scoreboards.Add(Player.Side.Left, scoreboardLeft);
        scoreboards.Add(Player.Side.Right, scoreboardRight);

        fader.Fade(0, 1);
        ballClone = ball.gameObject;
    }

    void Update() {
        if (ballSpawned == true && ballBounces % 5 != 0) {
            ballSpawned = false;
        }
    }

    // For multiplayer goals
    public void ScorePoint(Player.Side side) {
        score[side]++;

        if (score[side] >= Registry.maximumPoints) {
            EndGame(side);
        } 

        scoreboards[side].transform.Find(score[side].ToString()).GetComponent<SpriteRenderer>().enabled = true;
    }

    // For singleplayer ball goals
    public void RemoveLife() {
        if (BallsLeft > 0) {
            scoreboardSingleplayer.transform.Find(BallsLeft.ToString()).GetComponent<SpriteRenderer>().enabled = false;
            BallsLeft--;
            UpdateBounceCounter();

            if (BallsLeft <= 0) {
                EndGame();
            }
        }
    }

    public void UpdateBounceCounter(bool reset = false) {
        if (reset) {
            ballBounces = 0;
        } else { 
            ballBounces++;
        }

        counter.text = ballBounces.ToString();
    }

    private void EndGame(Player.Side side = Player.Side.Left) {
        Registry.gameInProgress = false;
        Camera.main.GetComponent<CameraZoom>().Zoom(8.6f, 1);
        iTween.MoveTo(Camera.main.gameObject, new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, -15), 1);
        fader.Fade(0.75f, 1);
        endingGUI.TriggerEnding(side);
        StartCoroutine(sfxCountdown(side));
    }

    public void ResetGame() {
        if (Registry.gameType == Registry.GameType.Multiplayer) { 
            score[Player.Side.Left] = 0;
            score[Player.Side.Right] = 0;

            foreach (Transform child in scoreboards[Player.Side.Left].transform) {
                child.GetComponent<SpriteRenderer>().enabled = false;
            }

            foreach (Transform child in scoreboards[Player.Side.Right].transform) {
                child.GetComponent<SpriteRenderer>().enabled = false;
            }
        } else if (Registry.gameType == Registry.GameType.Singleplayer) {
            BallsLeft = 5;
            ballBounces = 0;
            foreach (Transform child in scoreboardSingleplayer.transform) {
                child.GetComponent<SpriteRenderer>().enabled = true;
            }

            GameObject[] balls = GameObject.FindGameObjectsWithTag("Player");

            for (int i = 0; i < balls.Length; i++) {
                Destroy(balls[i]);
            }

            UpdateBounceCounter(true);
        }

        timer.time = 0;
        GameObject go = (GameObject)Instantiate(ballClone, new Vector3(0, 0, -3), Quaternion.identity);

        // ??
        go.GetComponent<Ball>().enabled = true;
        go.GetComponent<Ball>().Respawn();

        Registry.gameInProgress = true;
        Camera.main.GetComponent<CameraZoom>().Zoom(5, 1);
        iTween.MoveTo(Camera.main.gameObject, new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, -10), 1);
        fader.Fade(0, 1);
    }

    public IEnumerator sfxCountdown(Player.Side side) {
        for (float timer = 1; timer >= 0; timer -= Time.deltaTime)
            yield return 0;

        if (Registry.gameType == Registry.GameType.Multiplayer) {
            if (side == Player.Side.Left) {
                audioManager.blueWins.Play();
            } else {
                audioManager.redWins.Play();
            }
        } else if (Registry.gameType == Registry.GameType.Singleplayer) {
            audioManager.gameOver.Play();
        }
    }
}